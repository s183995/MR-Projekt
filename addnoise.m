function noisy_fft = addnoise(fft, tau)
%noisy_fft = addnoise(fft, tau)
% Tilf�jer st�j til et DFT-signal.
%Input:
% - fft - DFT-signalet der skal tilf�jes st�j til.
% - tau - St�jniveauet som decimaltal mellem 0 og 1 eller mellem 1 og 100.
%Output:
% - noisy_fft - DFT-signalet med st�j tilf�jet.
[n, k] =  size(fft);
assert(tau <= 100 && tau >= 0, 'St�j-niveauet skal angives som enten et decimaltal(mellem 0 og 1) eller et tal mellem 0 og 100');
if(tau > 1)
    tau = tau/100;
end
if(isreal(fft))
    r = randn(n,k);
else
    r = randn(n,k)+1i*randn(n,k);
end
r = r/norm(r,'fro');
e = tau*r*norm(fft,'fro');
noisy_fft = e+fft;
end