function err = error_measure(orig,im)
%err = error_measure(orig,im)
%Input:
% - orig - Det originale billede (ground truth) som fejlm�let skal regnes
% ud fra.
% - im - Det billede som skal tjekkes for fejl p�.
    assert(nargin == 2, 'Ikke nok input-parametre.');
    assert(all(size(orig) == size(im)), 'Input-billederne skal have samme st�rrelse.');
    err = norm(orig-im,'fro')/norm(orig,'fro');
end