function image = generate_simdata(K, texFiles)
%%image = generate_simdata(K, texFiles)
%Generate Simulation Data
% Input:
% dimension of output image.
% the folder containing the texture files (optional)
%
% Output:
% Image with square, circle and triangle, with random positions and size
% depending on the dimension size. If the optional input, texFiles, is
% filled then the square, circle and triangle will have the texture
% input as background.
assert(nargin >= 1,'Dimensionen af billedet skal angives');
assert(~isnan(K) & K > 0 & mod(K,1) == 0, 'Dimensionen skal v�re et helt positivt tal.');
useTex = nargin == 2;
tile_set = zeros(K,K,3);
randorder = randperm(3);
if(useTex)
    assert(7==exist(texFiles,'dir'), 'Mappen findes ikke.');
    assert(ischar(texFiles),'Stien til teksturbillederne skal v�re en streng (char-array)');
    assert(contains(texFiles,'textureFiles'), strcat("Teksturbillederne ligger i en mappe kaldet textureFiles. Referer d�rtil. Pr�v med textureFiles/",string(texFiles)));
    path = dir(texFiles);
    folder = path.folder;
    files = [dir(strcat(folder,'\*.png')); dir(strcat(folder,'\*.jpg')); dir(strcat(folder,'\*.tiff'))];
    randorder = randperm(numel(files));
    for n = 1:3
        filePath = strcat(folder,"\",files(randorder(n)).name);
        assert(isfile(filePath), strcat("Filen ",filePath," eksisterer ikke."));
        [tex_load, map] = imread(filePath);
        %check if texture is an indexed image-file.
        if ~isempty(map)
            tex_load = double(ind2gray(tex_load,map))./255;
        else
            assert(size(tex_load,3) == 1, strcat("For mange farvekanaler i ",filePath));
            tex_load = double(tex_load)./255;
        end
        [tex_h, tex_w] = size(tex_load);
        %Copy the texture to the size of the dimension, K.
        big_tiles = repmat(tex_load,ceil(K/tex_h),ceil(K/tex_w));
        tile_set(:,:,n) = big_tiles(1:K,1:K);
    end
end



%Initalize triangle vars
tri_w = floor(K/5);
tri_x = ceil(randi([1, K-tri_w],1));
tri_y = ceil(randi([1, K-tri_w],1));
tri_im = zeros(K,K);
%Draw the triangle with 1/3 pixel brightness.
for n = 1:tri_w
    if(~useTex)
        tri_im(tri_x+n,tri_y+(1:n)) = 1/3;
    else
        %Index into tilesets without start_x and start_y for the shapes not to
        %have same texture at points.
        tri_im(tri_x+n,tri_y+(1:n)) = tile_set(tri_x+n,tri_y+(1:n),randorder(1))/3;
    end
end
%Initalize square vars
sqa_w = floor(K/5);
sqa_x = ceil(randi([1, K-sqa_w],1));
sqa_y = ceil(randi([1, K-sqa_w],1));
sqa_im = zeros(K,K);
%Draw the square with 1/3 pixel brightness.
if(~useTex)
    sqa_im(sqa_x:(sqa_x+sqa_w), sqa_y:(sqa_y+sqa_w)) = 1/3;
else
    sqa_im(sqa_x:(sqa_x+sqa_w), sqa_y:(sqa_y+sqa_w)) = tile_set(sqa_x:(sqa_x+sqa_w), sqa_y:(sqa_y+sqa_w),randorder(2))/3;
end

%Initalize circle vars
cir_r = floor(K/8);
cir_x = ceil(randi([1, K-2*cir_r],1))+cir_r;
cir_y = ceil(randi([1, K-2*cir_r],1))+cir_r;
cir_im = zeros(K,K);
%Draw the circle with 1/3 pixel brightness.
for x = (cir_x-cir_r):(cir_x+cir_r)
    for y = (cir_y-cir_r):(cir_y+cir_r)
        if((x-cir_x)^2+(y-cir_y)^2 < cir_r^2)
            if(~useTex)
                cir_im(x,y) = 1/3;
            else
                cir_im(x,y) = tile_set(x,y,randorder(3))/3;
            end
        end
    end
end
image = tri_im+sqa_im+cir_im;
end