function [fft2d, shifted] = showFFT(image, useLog, zero_pad, show, color)
%%2dfft = showFFT(image, shrink, show)
%Viser absolutværdien af den to-dimensionelle diskrete
%fouriertransformation af et billede, image.
% Input:
% image - billedet der skal transformeres.
% zero_pad - logisk variabel om hvorvidt billedet skal formindskes før
% transformationen.
% useLog - logisk variabel om logaritmen skal tages inden visning.
% show - logisk variable om plottet skal vises.
% color - logisk variabel om plottet skal skaleres i farver.
% Output:
% fft2d - Den to-dimensionelle diskrete fouriertransformation af billedet.
% shifted - Den shiftede transformation med absolutværdierne.
if(zero_pad)
fft2d = fft2(image,2^nextpow2(size(image,1)),2^nextpow2(size(image,2)));
else
fft2d = fft2(image);
end
if(useLog)
   shifted = log(abs(fftshift(fft2d))); 
else
    shifted = abs(fftshift(fft2d));
end
if(show && color)
   imagesc(shifted); 
elseif(show && ~color)
   imagesc(shifted);
   colormap(gray);
end
end