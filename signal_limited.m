function dft = signal_limited(signal, frac)
%dft = signal_limited(signal, frac)
%Fjerner der h�jeste frekvenser fra et signal, ved at beholde en andel af
%de dybe.
%For istedet at fjerne de lave frekvenser, fftshift inden funktionen kaldes
%og derefter fftshift igen f�r rekonstruering.
%Input:
% - signal - Det fulde DFT-signal.
% - frac - Br�kdelen af det fulde DFT-signal som skal bibeholdes.
%Output:
% - dft - DFT-signalet hvor br�kdelen angivet er fjernet i kanten.
% Skal imitere en mindre samplingst�rrelse. Dette er ikke zero-padding, da
% zero-padding er hvor man tilf�jer nuller til et datas�t.
% Effektiviteten af FFT-funktionen kunne �ges ved at oprunde dimensionerne
% af DFT-signalet til en faktor af 2. Dermed kan radix-2-FFT-algoritmer
% bruges, som er meget mere effektive.
    [h, w, s] = size(signal);
    assert(frac < 1 && frac > 0, 'Br�kdelen der skal beholdes skal v�re et tal mellem 0 og 1.')
    h_padding = ceil((1-frac)/2*h);
    w_padding = ceil((1-frac)/2*w);
    dft = zeros(h,w,s);
    shifted_signal = fftshift(signal);
    for n = 1:s
       dft(h_padding:(end-h_padding),w_padding:(end-w_padding),n) =  shifted_signal(h_padding:(end-h_padding),w_padding:(end-w_padding),n);
    end
    dft = fftshift(dft);
end